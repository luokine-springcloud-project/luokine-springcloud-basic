package com.luokine.basic.core.service;

import com.luokine.basic.entity.bean.UserLogin;

import com.luokine.common.orm.mapper.base.CommonService;

/**
* 通用 service 代码生成器
*
* @author
*/
public interface UserLoginService extends CommonService<UserLogin , Integer> {

}



