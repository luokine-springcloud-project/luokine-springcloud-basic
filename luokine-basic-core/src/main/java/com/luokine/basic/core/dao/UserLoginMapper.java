package com.luokine.basic.core.dao;

import com.luokine.basic.entity.bean.UserLogin;

import com.luokine.common.orm.mapper.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* 通用 Mapper 代码生成器
*
* @author
*/
@Mapper
public interface UserLoginMapper extends BaseMapper<UserLogin , Integer> {

}
