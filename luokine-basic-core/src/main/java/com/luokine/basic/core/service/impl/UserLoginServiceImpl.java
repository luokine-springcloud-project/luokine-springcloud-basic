package com.luokine.basic.core.service.impl;

import com.luokine.basic.entity.bean.UserLogin;

import com.luokine.common.orm.mapper.base.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;
import com.luokine.basic.core.dao.UserLoginMapper;
import com.luokine.basic.core.service.UserLoginService;

/**
* 通用 serviceImpl 代码生成器
*
* @author
*/
@Service
public class UserLoginServiceImpl extends CommonServiceImpl<UserLoginMapper , UserLogin , Integer> implements UserLoginService{

}



