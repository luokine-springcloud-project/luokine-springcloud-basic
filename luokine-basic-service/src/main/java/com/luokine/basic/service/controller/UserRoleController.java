package com.luokine.basic.service.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.luokine.basic.core.dao.UserLoginMapper;
import com.luokine.basic.core.service.UserLoginService;
import com.luokine.basic.core.service.UserRoleService;
import com.luokine.basic.entity.bean.UserLogin;
import com.luokine.basic.entity.bean.UserRole;
import com.luokine.basic.entity.vo.UserLoginPageRespvo;
import com.luokine.basic.entity.vo.UserRoleReqVo;
import com.luokine.common.model.Vo.Resp;
import com.luokine.common.orm.page.PageHelperObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author: tiantziquan
 * @create: 2019-10-25 18:24
 */
@RestController
@Slf4j
@RequestMapping("/basic")
@Api(tags = "基础调用provider")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private UserLoginService userLoginService;
    @Autowired
    private UserLoginMapper userLoginMapper;

    @PostMapping("/getRolePage")
    @ApiOperation("分页获取角色 resp")
    public Resp<IPage<UserRole>> getRolePage(@RequestBody UserRoleReqVo vo) {
        IPage<UserRole> userRolePage = userRoleService.getUserRolePage(vo);
        return Resp.ok(userRolePage);
    }

    @PostMapping("/getRolePageList")
    @ApiOperation("分页获取角色 Resp list")
    public Resp<List<UserRole>> getRolePageList(@RequestBody UserRoleReqVo vo) {
        List<UserRole> list = userRoleService.list();
        return Resp.ok(list);
    }

    @PostMapping("/getRolePageList2")
    @ApiOperation("获取角色 list")
    public List<UserRole> getRoleList(@RequestBody UserRoleReqVo vo) {
        List<UserRole> list = userRoleService.list();
        return list;
    }

    @GetMapping("/getRoleById")
    @ApiOperation("获取角色 id")
    public UserRole getRoleById(@RequestParam Integer id) {
        UserRole user = userRoleService.getUserRoleById(id);
//        UserLogin userLogin = new UserLogin();
//        Example example = new Example(UserLogin.class);
//        Example.Criteria criteria = example.createCriteria();
//        criteria.andEqualTo(UserLogin.ID,1);
//        UserLogin one = userLoginService.getOne(example);
//        System.out.println("one = " + one);
        return user;
    }

    @GetMapping("/pageList")
    @ApiOperation("分页查询")
    public Resp<PageInfo<UserLoginPageRespvo>> testPageList() {
        PageHelperObject pageHelperObject = new PageHelperObject(1, 2);
        Example example = new Example(UserLogin.class);
        Example.Criteria criteria = example.createCriteria();
        List<Integer> idList = Arrays.asList(new Integer[]{1, 2, 3, 4});
        criteria.andIn(UserLogin.ID, idList);
        example.setOrderByClause(UserLogin.DB_ID + " DESC");
        PageInfo<UserLogin> pageInfo = userLoginService.selectByExampleAndPageHelper(example, pageHelperObject);
        List<UserLogin> list = pageInfo.getList();
        List<UserLoginPageRespvo> voList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(list)) {
            for (UserLogin userLogin : list) {
                UserLoginPageRespvo vo = new UserLoginPageRespvo();
                BeanUtils.copyProperties(userLogin, vo);
                voList.add(vo);
            }
        }
        PageInfo<UserLoginPageRespvo> pageVo = new PageInfo<>();
        BeanUtils.copyProperties(pageInfo, pageVo);
        pageVo.setList(voList);
        return Resp.ok(pageVo);
    }

    @GetMapping("/test")
    @ApiOperation("测试")
    public Resp<Integer> getRoleById() {
        Example example = new Example(UserLogin.class);
        Example.Criteria criteria = example.createCriteria();
        List<Integer> idList = Arrays.asList(new Integer[]{1, 2, 3, 4});
        criteria.andIn(UserLogin.ID, idList);
        List<UserLogin> list = userLoginService.list(example);
        System.out.println("list = " + list);
        UserLogin userLogin = new UserLogin();
        userLogin.setId(2);
        userLogin.setPassword("123456");
        boolean b = userLoginService.updateById(userLogin);
        System.out.println("b = " + b);
//        boolean update = userLoginService.updateById(userLogin);
        userLogin.setId(1);
        int update = userLoginMapper.updateByPrimaryKeySelective(userLogin);
        log.info("测试修改：{}", JSON.toJSONString(update));
        return Resp.of(200, "获取测试数据成功", update);
    }

    @GetMapping("/testInsert")
    @ApiOperation("测试主键")
    public Resp<Boolean> testInsert() {
        UserLogin userLogin = new UserLogin();
        userLogin.setMobileNo("1111");
        userLogin.setPassword("123456");
        boolean save = userLoginService.save(userLogin);
        Integer id = userLogin.getId();
        System.out.println("id = " + id);
        log.info("测试修改：{}", JSON.toJSONString(userLogin));
        return Resp.of(200, "获取测试数据成功", true);
    }

}
