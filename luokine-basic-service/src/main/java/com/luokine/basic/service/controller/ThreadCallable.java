package com.luokine.basic.service.controller;

import com.alibaba.fastjson.JSON;
import com.luokine.common.model.Vo.Resp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

/**
 * @author: tianziquan
 */
@RestController
@RequestMapping("/basic/threadCallable")
@Slf4j
@Api(tags = "测试多线程")
public class ThreadCallable {

    @GetMapping("/testThreadCallable")
    @ApiOperation("测试多线程接口")
    public Callable<Resp<Boolean>> testThreadCallable(@RequestParam Long time) throws InterruptedException {
        return () -> {
            Thread.sleep(time);
            log.info("time ={}", JSON.toJSONString(time));
            log.info("time2 ={}", JSON.toJSONString(time));
            return Resp.ok(true);
        };

    }
}
