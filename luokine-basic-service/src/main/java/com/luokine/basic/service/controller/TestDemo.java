package com.luokine.basic.service.controller;

import com.alibaba.fastjson.JSON;
import com.luokine.basic.entity.vo.TestVolidaBigci;
import com.luokine.common.model.Vo.Resp;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author: tianziquan
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestDemo {
    @PostMapping("/testValidator")
    @ApiOperation("测试校验BigDecimal")
    public Resp<String> testValidator(@Valid @RequestBody TestVolidaBigci vo) {
        log.info("入参->[{}]", JSON.toJSONString(vo));
        return Resp.ok("success");
    }
}
