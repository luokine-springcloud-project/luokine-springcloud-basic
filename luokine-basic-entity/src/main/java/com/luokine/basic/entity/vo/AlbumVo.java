package com.luokine.basic.entity.vo;

import lombok.Data;

/**
 * @author: tianziquan
 */
@Data
public class AlbumVo {
    private Integer albumId;
    private String albumName;
}
