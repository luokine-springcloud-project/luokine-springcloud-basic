package com.luokine.basic.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author: tianziquan
 * @create: 2019-12-20 15:37
 */
@Data
@ApiModel(description = "测试分页返回列表")
public class UserLoginPageRespvo {
    private Integer id;

    private String mobileNo;

    private String password;
}
