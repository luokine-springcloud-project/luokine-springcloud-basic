package com.luokine.basic.entity.vo;

import lombok.Data;

/**
 * @author: tianziquan
 */
@Data
public class UserLoginVO {
    private Integer loginId;
    private String password;
}
