package com.luokine.basic.entity.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luokine.basic.entity.bean.UserRole;

/**
 * @author: tiantziquan
 * @create: 2019-10-25 18:13
 */
public class UserRoleReqVo extends Page<UserRole> {
    private Integer roleId;
}
