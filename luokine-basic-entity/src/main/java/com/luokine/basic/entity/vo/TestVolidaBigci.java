package com.luokine.basic.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * @author: tianziquan
 * @create: 2019-12-20 15:37
 */
@Data
@ApiModel(description = "测试入参最小值")
public class TestVolidaBigci {
    @ApiModelProperty(value = "测试",required = true)
    @DecimalMin(value = "0.0001",message = "必须大于0")
    private BigDecimal test;
}
